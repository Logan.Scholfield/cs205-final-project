import pygame
from constants import *


'''
used to display information about the game to the user
'''
class Instructions:

    '''
    init function that takes in the window that the text will be displayed to
    '''
    def __init__(self, window):
        self.window = window
        self.title_main_font = pygame.font.Font('freesansbold.ttf', 45)

        self.title_font = pygame.font.Font('freesansbold.ttf', 28)
        self.sentence_font = pygame.font.Font('freesansbold.ttf', 15)
        self.text = None

        self.title = 'Instructions'

        self.solo_title = 'Solo'
        self.solo_instructions1 = 'Number of players: 1'
        self.solo_instructions2 = 'Objective: Survive and get the longest snake you can'

        self.co_op_title = 'Co-op'
        self.co_op_instructions1 = 'Number of players: 2'
        self.co_op_instructions2 = 'Objective: Work with your partner to survive as long'
        self.co_op_instructions3 = 'you both can together to get the highest score'

        self.versus_title = 'Versus'
        self.versus_instructions1 = 'Number of players: 2'
        self.versus_instructions2 = 'Objective: Compete to see who can survive to get the'
        self.versus_instructions3 = 'longest snake'

        self.powerup_instructions = 'Powerups:'

        self.powerup_bomb_title = 'Bomb'
        self.powerup_bomb_instructions1 = 'If you hit the bomb you will die'
        self.powerup_bomb_instructions2 = 'The bomb will disapear after 10 seconds'

        self.powerup_food_title = 'Double food'
        self.powerup_food_instructions1 = 'If you collect double food you will'
        self.powerup_food_instructions2 = 'receive double food for 10 seconds'

        self.food_title = 'Food:'
        self.food_instructions1 = 'food will be colored squares'
        self.food_instructions2 = 'collect the food to get longer'




    '''
    function called in main to display all instruction information to the user
    no unputs or returns, function is strictly for display of the screen
    '''
    def display(self):

        ''''--------------------title Text--------------------'''
        self.text = self.title_main_font.render(self.title, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.center = (dis_width / 2, 50)
        self.window.blit(self.text, self.text_background)


        ''''--------------------solo Text--------------------'''
        self.text = self.title_font.render(self.solo_title, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / title_indent, 100)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.solo_instructions1, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / sentence_indent, 140)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.solo_instructions2, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / sentence_indent, 160)
        self.window.blit(self.text, self.text_background)


        ''''--------------------co-op Text--------------------'''
        self.text = self.title_font.render(self.co_op_title, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / title_indent, 200)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.co_op_instructions1, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / sentence_indent, 240)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.co_op_instructions2, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / sentence_indent, 260)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.co_op_instructions3, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / sentence_indent, 280)
        self.window.blit(self.text, self.text_background)


        ''''--------------------Versus Text--------------------'''
        self.text = self.title_font.render(self.versus_title, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / title_indent, 320)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.versus_instructions1, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / sentence_indent, 360)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.versus_instructions2, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / sentence_indent, 380)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.versus_instructions3, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (dis_width / sentence_indent, 400)
        self.window.blit(self.text, self.text_background)


        ''''--------------------powerup Text--------------------'''
        self.text = self.title_font.render(self.powerup_instructions, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (550, 100)
        self.window.blit(self.text, self.text_background)


        ''''--------------------bomb Text--------------------'''
        self.text = self.title_font.render(self.powerup_bomb_title, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (580, 140)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.powerup_bomb_instructions1, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (600, 180)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.powerup_bomb_instructions2, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (600, 200)
        self.window.blit(self.text, self.text_background)

        pygame.draw.circle(self.window, (0, 0, 0), [725 + (block_size / 2), 140 + (block_size / 2)], block_size / 2, 0)
        pygame.draw.rect(self.window, bomb_yellow, pygame.Rect(725 + ((block_size / 2) - (block_size / 8)), 140, block_size / 4, block_size / 4))

        ''''--------------------double food Text--------------------'''
        self.text = self.title_font.render(self.powerup_food_title, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (580, 240)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.powerup_food_instructions1, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (600, 280)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.powerup_food_instructions2, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (600, 300)
        self.window.blit(self.text, self.text_background)

        pygame.draw.rect(self.window, canvas_tan, pygame.Rect(775, 240, block_size, block_size))
        pygame.draw.rect(self.window, bomb_yellow,
                         pygame.Rect(775 + ((block_size / 2) - (block_size / 8)), 240, mini_block,
                                     mini_block))
        pygame.draw.rect(self.window, bomb_yellow,
                         pygame.Rect(775 + ((block_size / 2) - (block_size / 8)), 240 + mini_block,
                                     mini_block, mini_block))
        pygame.draw.rect(self.window, bomb_yellow,
                         pygame.Rect(775 + ((block_size / 2) - (block_size / 8)), 240 + mini_block * 2,
                                     mini_block, mini_block))
        pygame.draw.rect(self.window, bomb_yellow,
                         pygame.Rect(775 + ((block_size / 2) - (block_size / 8)), 240 + mini_block * 3,
                                     mini_block, mini_block))
        pygame.draw.rect(self.window, bomb_yellow,
                         pygame.Rect(775 + ((block_size / 2) - (block_size / 8)) + mini_block,
                                     240 + mini_block, mini_block, mini_block))
        pygame.draw.rect(self.window, bomb_yellow,
                         pygame.Rect(775 + ((block_size / 2) - (block_size / 8)) - mini_block,
                                     240 + mini_block, mini_block, mini_block))
        pygame.draw.rect(self.window, bomb_yellow,
                         pygame.Rect(775 + ((block_size / 2) - (block_size / 8)) + mini_block * 2,
                                     240 + mini_block * 2, mini_block / 2, mini_block))
        pygame.draw.rect(self.window, bomb_yellow,
                         pygame.Rect(775 + ((block_size / 2) - (block_size / 8)) - mini_block * 1.5,
                                     240 + mini_block * 2, mini_block / 2, mini_block))

        ''''--------------------food Text--------------------'''
        self.text = self.title_font.render(self.food_title, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (550, 340)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.food_instructions1, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (600, 380)
        self.window.blit(self.text, self.text_background)

        self.text = self.sentence_font.render(self.food_instructions2, True, black)
        self.text_background = self.text.get_rect()
        self.text_background.topleft = (600, 400)
        self.window.blit(self.text, self.text_background)








