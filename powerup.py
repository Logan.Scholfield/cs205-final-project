from constants import *
from snake import *
import pygame
import random
import time

pygame.init()
powerup_sound = pygame.mixer.Sound('Eat Sound 1.wav')

'''
Powerup class used in all modes
'''
class Powerup:
    '''
    takes in window to be displayed on and sets a lot of defaults
    '''
    def __init__(self, window):
        self.type = self.get_random_type()
        self.appear = False
        self.loc = self.get_loc()
        self.window = window
        self.time_double_food = None
        self.time_bomb = None
        self.exploding = False
        self.explosion_loc = [0,0]
        self.explosion_time = None
        self.exploded = False

    '''
    returns the 2 index array for the x and y cords
    '''
    def get_loc(self):
        loc = [round(random.randrange(0, (size[0]-block_size))), round(random.randrange(0, (size[1]-block_size)))]
        loc[0] = round(loc[0]/block_size)*block_size
        loc[1] = round(loc[1]/block_size)*block_size
        return loc

    '''
    sets snakes food to double when collecting a double food object
    '''
    def double_food(self,snakeHead,snakeList):
        print('SETTING!')
        snakeHead.set_double_food(True)

    '''
    sets explosion in effect when a snake hits a bomb
    '''
    def bomb(self,snakeHead,snakeList):
        print("BOMB")
        self.exploding = True
        self.explosion_loc = snakeHead.get_loc()
        self.explosion_time = time.time()

    '''
    sets one of the powerup functions at random
    '''
    def get_random_type(self):
        rand_type = random.randint(1,number_powerups)
        if rand_type == 1:
            self.type = 'double_food'
        elif rand_type == 2:
            self.type = 'bomb'

    '''
    checks if a snake is touching a powerup and acts accordingly
    '''
    def touch(self, snake_loc, snakeHead, snakeList):
        if snake_loc == self.loc:
            self.appear = False
            if self.type == 'double_food':
                pygame.mixer.Sound.play(powerup_sound)
                self.double_food(snakeHead,snakeList)
                self.time_double_food = time.time()
            elif self.type == 'bomb':
                snakeHead.isAlive = False
                self.bomb(snakeHead,snakeList)

    '''
    checks if snake has double food for 10 seconds and removes the effect after time
    '''
    def check_double_food_time(self, snakeHead):
        if snakeHead.double_food:
            if time.time() - self.time_double_food > 10:
                snakeHead.set_double_food(False)


    '''
    maintains important features for powerups such as keeping track of time and spawning and look of powerups
    '''
    def initialize(self):
        if self.exploding:
            img = pygame.image.load(f'images/explosion.png')
            img = pygame.transform.scale(img, (300, 300))
            self.window.blit(img, (self.get_explosion_loc()[0] - 150, self.get_explosion_loc()[1] - 150))
            if (self.check_time(self.explosion_time) > 0.5):
                self.exploding = False
                self.exploded = True
        if self.appear:
            if self.type == 'double_food':
                pygame.draw.rect(self.window, canvas_tan, pygame.Rect(self.loc[0], self.loc[1], block_size, block_size))
                pygame.draw.rect(self.window, bomb_yellow, pygame.Rect(self.loc[0] + ((block_size/2) - (block_size/8)), self.loc[1], mini_block, mini_block))
                pygame.draw.rect(self.window, bomb_yellow, pygame.Rect(self.loc[0] + ((block_size/2) - (block_size/8)), self.loc[1]+ mini_block, mini_block, mini_block))
                pygame.draw.rect(self.window, bomb_yellow, pygame.Rect(self.loc[0] + ((block_size/2) - (block_size/8)), self.loc[1]+ mini_block*2, mini_block, mini_block))
                pygame.draw.rect(self.window, bomb_yellow, pygame.Rect(self.loc[0] + ((block_size/2) - (block_size/8)), self.loc[1] + mini_block*3, mini_block, mini_block))


                pygame.draw.rect(self.window, bomb_yellow, pygame.Rect(self.loc[0] + ((block_size/2) - (block_size/8)) + mini_block, self.loc[1] + mini_block, mini_block, mini_block))
                pygame.draw.rect(self.window, bomb_yellow, pygame.Rect(self.loc[0] + ((block_size/2) - (block_size/8)) - mini_block, self.loc[1] + mini_block, mini_block, mini_block))
                pygame.draw.rect(self.window, bomb_yellow,pygame.Rect(self.loc[0] + ((block_size / 2) - (block_size / 8)) + mini_block*2,self.loc[1] + mini_block*2, mini_block/2, mini_block))
                pygame.draw.rect(self.window, bomb_yellow,pygame.Rect(self.loc[0] + ((block_size / 2) - (block_size / 8)) - mini_block*1.5,self.loc[1] + mini_block*2, mini_block/2, mini_block))

            if self.type == 'bomb':
                if self.check_time(self.time_bomb) > 10.0:
                    self.appear = False
                pygame.draw.circle(self.window, (0, 0, 0),[self.loc[0] + (block_size/2), self.loc[1] + (block_size/2)], block_size/2, 0)
                pygame.draw.rect(self.window, bomb_yellow, pygame.Rect(self.loc[0] + ((block_size/2) - (block_size/8)), self.loc[1], block_size/4, block_size/4))
        else:
            self.get_random_type()
            if self.type == 'bomb':

                self.time_bomb = time.time()
            self.loc = self.get_loc()
            self.appear = True


    '''
    checks curent time based on inputed time and returns total difference in time
    '''
    def check_time(self, input_time):
        time_2 = time.time()
        print(time.time() - input_time)
        return (time.time() - input_time)


    '''
    returns the location of the explosion and returns value
    '''
    def get_explosion_loc(self):
        print(self.explosion_loc)
        return self.explosion_loc



