def makeButton(fontSize, text, textColor, boxColor, highlightedBoxColor, xLocation, yLocation, buttonHeight,buttonWidth, dis_width, dis_height,pygame,dis):
    # Create text
    mouse = pygame.mouse.get_pos()
    font = pygame.font.Font('freesansbold.ttf', fontSize)
    textBox = font.render(text, True, boxColor)
    text_block = textBox.get_rect()
    # Account for the length of the string and font size when rendering where the text is
    text_block.center = xLocation + 0.1*fontSize*(8 - len(text))*(fontSize/50), yLocation

    # Draw different colored boxes depending upon mouse location
    if xLocation - 0.45 * buttonWidth <= mouse[
        0] <= xLocation + 0.55 * buttonWidth and yLocation - 0.5 * buttonHeight <= mouse[
        1] <= yLocation + 0.5 * buttonHeight:
        pygame.draw.rect(dis, highlightedBoxColor,
                         [(xLocation) - 0.45 * buttonWidth, yLocation - 0.5 * buttonHeight, buttonWidth, buttonHeight])
        textRender = font.render(text, True, textColor)

    else:
        pygame.draw.rect(dis, boxColor,[xLocation - 0.45 * buttonWidth, yLocation - 0.5 * buttonHeight, buttonWidth, buttonHeight])
        textRender = font.render(text, True, textColor)

    dis.blit(textRender, text_block)