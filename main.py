import pygame.mixer

from instructions import *
from food import *
from background import *
from powerup import *

pygame.init()
from graphicsFunctions import *
from pygame import mixer

mixer.music.load('Nightsong Strings.wav')
if play_music:
    mixer.music.play(-1)

click_sound = pygame.mixer.Sound('Click Noise.wav')
snake_death = pygame.mixer.Sound('Snake Death.wav')

dis = pygame.display.set_mode((c.dis_width, c.dis_height))
pygame.display.set_caption('Snake Game')

background = Background(dis)
instructions = Instructions(dis)

powerup1 = Powerup(dis)

snake1IsActive = True
snake2IsActive = True

clock = pygame.time.Clock()

snakeHead1 = Snake(white, snake1IsActive, dis)
snakeList1 = []
snakeList1.append(snakeHead1)

snakeHead2 = Snake(white, snake2IsActive, dis)
snakeList2 = []
snakeList2.append(snakeHead2)

food1 = Food(dis)
game_started = False

while not game_over:
    if screen == 'start':
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                game_over = True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    screen = 'game'
                    print(screen)

            if event.type == pygame.MOUSEBUTTONDOWN and soloButtonXlocation - 0.45 * soloButtonWidth <= mouse[
                0] <= soloButtonXlocation + 0.55 * soloButtonWidth and soloButtonYlocation - 0.5 * soloButtonHeight <= \
                    mouse[1] <= soloButtonYlocation + 0.5 * soloButtonHeight:
                screen = 'solo'
                game_started = False
                countdown = countdown_duration
                snakeHead1.Set_Direction(0, 0)
                snakeHead1.x = dis_width / 2
                snakeHead1.y = dis_height / 2
                pygame.mixer.Sound.play(click_sound)
                print(screen)

            if event.type == pygame.MOUSEBUTTONDOWN and coopButtonXlocation - 0.45 * coopButtonWidth <= mouse[
                0] <= coopButtonXlocation + 0.55 * coopButtonWidth and coopButtonYlocation - 0.5 * coopButtonHeight <= \
                    mouse[1] <= coopButtonYlocation + 0.5 * coopButtonHeight:
                screen = 'coop'
                game_started = False
                countdown = countdown_duration
                snakeHead1.Set_Queued_Direction(-snake_speed, 0)
                snakeHead2.Set_Queued_Direction(snake_speed, 0)
                snakeHead1.x = dis_width - 175
                snakeHead1.y = dis_height / 2
                snakeHead2.x = 175
                snakeHead2.y = dis_height / 2
                pygame.mixer.Sound.play(click_sound)
                print(screen)

            if event.type == pygame.MOUSEBUTTONDOWN and versusButtonXlocation - 0.45 * versusButtonWidth <= mouse[
                0] <= versusButtonXlocation + 0.55 * versusButtonWidth and versusButtonYlocation - 0.5 * versusButtonHeight <= \
                    mouse[1] <= versusButtonYlocation + 0.5 * versusButtonHeight:
                screen = 'versus'
                game_started = False
                countdown = countdown_duration
                snakeHead1.x = dis_width - 175
                snakeHead1.y = dis_height / 2
                snakeHead2.x = 175
                snakeHead2.y = dis_height / 2
                snakeHead1.Set_Queued_Direction(-1, 0)
                snakeHead2.Set_Queued_Direction(1, 0)
                pygame.mixer.Sound.play(click_sound)
                print(screen)

            if event.type == pygame.MOUSEBUTTONDOWN and shopButtonXlocation - 0.45 * shopButtonWidth <= mouse[
                0] <= shopButtonXlocation + 0.55 * shopButtonWidth and shopButtonYlocation - 0.5 * shopButtonHeight <= \
                    mouse[1] <= shopButtonYlocation + 0.5 * shopButtonHeight:
                pygame.mixer.Sound.play(click_sound)
                screen = 'shop'

            if event.type == pygame.MOUSEBUTTONDOWN and helpButtonXlocation - 0.45 * helpButtonWidth <= mouse[
                0] <= helpButtonXlocation + 0.55 * helpButtonWidth and helpButtonYlocation - 0.5 * helpButtonHeight <= \
                    mouse[1] <= helpButtonYlocation + 0.5 * helpButtonHeight:
                pygame.mixer.Sound.play(click_sound)
                screen = 'help'

        # Create title
        font = pygame.font.Font('freesansbold.ttf', 65)
        text = font.render('Snake Game', True, button_color)
        text_block = text.get_rect()
        text_block.center = dis_width / 2, dis_height / 7

        # Create gold count text
        goldFont = pygame.font.Font('freesansbold.ttf', 20)
        gold_msg = goldFont.render("Gold: " + str(gold), True, gold_color)

        dis.fill((61, 124, 184))

        # Make buttons
        makeButton(50, "Co-op", button_text_color, button_color, highlighted_button_color, coopButtonXlocation, coopButtonYlocation,
                   coopButtonHeight,
                   coopButtonWidth, dis_width, dis_height, pygame, dis)

        makeButton(50, "Shop", button_text_color, button_color, highlighted_button_color, shopButtonXlocation, shopButtonYlocation,
                   shopButtonHeight,
                   shopButtonWidth, dis_width, dis_height, pygame, dis)

        makeButton(50, "Solo", button_text_color, button_color, highlighted_button_color, soloButtonXlocation, soloButtonYlocation,
                   soloButtonHeight,
                   soloButtonWidth, dis_width, dis_height, pygame, dis)

        makeButton(50, "Versus", button_text_color, button_color, highlighted_button_color, versusButtonXlocation, versusButtonYlocation,
                   versusButtonHeight,
                   versusButtonWidth, dis_width, dis_height, pygame, dis)

        makeButton(25, "Help", button_text_color, button_color, highlighted_button_color, helpButtonXlocation, helpButtonYlocation,
                   helpButtonHeight, helpButtonWidth, dis_width, dis_height, pygame, dis)

        # Create mouse object to track mouse position
        mouse = pygame.mouse.get_pos()

        # Render text and update display
        dis.blit(gold_msg, (25, 25))
        dis.blit(text, text_block)
        pygame.display.update()


    elif screen == 'help':
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                game_over = True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    screen = 'game'
                    print(screen)

            if event.type == pygame.MOUSEBUTTONDOWN and helpButtonXlocation - 0.45 * helpButtonWidth <= mouse[
                0] <= helpButtonXlocation + 0.55 * helpButtonWidth and helpButtonYlocation - 0.5 * helpButtonHeight <= \
                    mouse[1] <= helpButtonYlocation + 0.5 * helpButtonHeight:
                screen = 'start'
                pygame.mixer.Sound.play(click_sound)

        instructions.display()

        makeButton(25, "Back", black, red, light_red, helpButtonXlocation, helpButtonYlocation,
                   helpButtonHeight, helpButtonWidth, dis_width / 2, 450, pygame, dis)

        mouse = pygame.mouse.get_pos()

        pygame.display.update()
        dis.fill(canvas_tan)


    elif screen == 'coop':
        score = snakeHead1.length + snakeHead2.length - 2
        if game_started:
            # only get input when game has started
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_over = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        snakeHead1.Set_Queued_Direction(-snake_speed, 0)
                    elif event.key == pygame.K_RIGHT:
                        snakeHead1.Set_Queued_Direction(snake_speed, 0)
                    elif event.key == pygame.K_UP:
                        snakeHead1.Set_Queued_Direction(0, -snake_speed)
                    elif event.key == pygame.K_DOWN:
                        snakeHead1.Set_Queued_Direction(0, snake_speed)

                    elif event.key == pygame.K_a:
                        snakeHead2.Set_Queued_Direction(-snake_speed, 0)
                    elif event.key == pygame.K_d:
                        snakeHead2.Set_Queued_Direction(snake_speed, 0)
                    elif event.key == pygame.K_w:
                        snakeHead2.Set_Queued_Direction(0, -snake_speed)
                    elif event.key == pygame.K_s:
                        snakeHead2.Set_Queued_Direction(0, snake_speed)

            # Makes all snake objects in snakeList follow the snake object in front of them
            for i in reversed(range(snakeHead1.length)):
                if i > 0:
                    snakeList1[i].follow(snakeList1[i - 1])

            for i in reversed(range(snakeHead2.length)):
                if i > 0:
                    snakeList2[i].follow(snakeList2[i - 1])

            # only move the snake when the game has started
            for snake in snakeList1:
                snake.move()
            for snake in snakeList2:
                snake.move()

        dis.fill(white)
        background.load_background()

        # Draw all snake objects
        for snake in snakeList1:
            snake.draw()
        for snake in snakeList2:
            snake.draw()

        if score % powerup_frequency == 0 and score != 0:
            powerup1.initialize()

        if powerup1.appear:
            powerup1.initialize()
            powerup1.touch([snakeHead1.x, snakeHead1.y], snakeHead1, snakeList1)
            powerup1.touch([snakeHead2.x, snakeHead2.y], snakeHead2, snakeList2)

        powerup1.check_double_food_time(snakeHead1)

        if powerup1.exploded:
            game_over = True

        # Collision detection for snake and window edge
        if snakeHead1.x >= dis_width or snakeHead1.x < 0 or snakeHead1.y >= dis_height or snakeHead1.y < 0:
            screen = 'end'
            pygame.mixer.Sound.play(snake_death)

        if snakeHead2.x >= dis_width or snakeHead2.x < 0 or snakeHead2.y >= dis_height or snakeHead2.y < 0:
            screen = 'end'
            pygame.mixer.Sound.play(snake_death)


        # Collision detection for a snake and itself
        for i in range(snakeHead1.length):
            if i != 0 and snakeList1[i].x == snakeList1[0].x and snakeList1[i].y == snakeList1[0].y:
                screen = 'end'
                pygame.mixer.Sound.play(snake_death)

        for i in range(snakeHead2.length):
            if i != 0 and snakeList2[i].x == snakeList2[0].x and snakeList2[i].y == snakeList2[0].y:
                screen = 'end'
                pygame.mixer.Sound.play(snake_death)

        # Collision detection for two snakes
        for i in range(snakeHead2.length):
            if snakeList2[i].x == snakeList1[0].x and snakeList2[i].y == snakeList1[0].y:
                screen = 'end'
                pygame.mixer.Sound.play(snake_death)

        for i in range(snakeHead1.length):
            if snakeList1[i].x == snakeList2[0].x and snakeList1[i].y == snakeList2[0].y:
                screen = 'end'
                pygame.mixer.Sound.play(snake_death)

        food1.touch([snakeHead1.x, snakeHead1.y], snakeHead1, snakeList1, )

        food1.touch([snakeHead2.x, snakeHead2.y], snakeHead2, snakeList2, )

        # Updates Score
        score_msg = font.render(str(score), True, blue)
        score_msg.set_alpha(130)
        dis.blit(score_msg, (19, 25))

        # display countdown
        if (not game_started):
            countdown_text = font.render(str(countdown), True, white)
            dis.blit(countdown_text, (dis_width / 2.02, dis_height / 2.12))

        pygame.display.update()

        # if countdown is greater than 0, wait and decrement countdown
        if (countdown > 0):
            time.sleep(1)
            countdown -= 1
        else:
            game_started = True

        clock.tick(125)


    elif screen == 'solo':
        score = snakeHead1.length - 1
        if game_started:
            # only get input when game has started
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_over = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        snakeHead1.Set_Queued_Direction(-snake_speed, 0)
                    elif event.key == pygame.K_RIGHT:
                        snakeHead1.Set_Queued_Direction(snake_speed, 0)
                    elif event.key == pygame.K_UP:
                        snakeHead1.Set_Queued_Direction(0, -snake_speed)
                    elif event.key == pygame.K_DOWN:
                        snakeHead1.Set_Queued_Direction(0, snake_speed)

            # Makes all snake objects in snakeList follow the snake object in front of them
            for i in reversed(range(snakeHead1.length)):
                if i > 0:
                    snakeList1[i].follow(snakeList1[i - 1])

            # Moves snake once game starts
            for snake in snakeList1:
                snake.move()

        dis.fill(white)
        background.load_background()

        for snake in snakeList1:
            snake.draw()

        if score % powerup_frequency == 0 and score != 0:
            powerup1.initialize()

        if powerup1.appear:
            powerup1.initialize()
            powerup1.touch([snakeHead1.x, snakeHead1.y], snakeHead1, snakeList1)

        powerup1.check_double_food_time(snakeHead1)

        if powerup1.exploded:
            screen = 'end'
            pygame.mixer.Sound.play(snake_death)

        # Collision detection for snake and window edge
        if snakeHead1.x >= dis_width or snakeHead1.x < 0 or snakeHead1.y >= dis_height or snakeHead1.y < 0:
            screen = 'end'
            pygame.mixer.Sound.play(snake_death)



        # Collision detection for a snake and itself
        for i in range(snakeHead1.length):
            if i != 0 and snakeList1[i].x == snakeList1[0].x and snakeList1[i].y == snakeList1[0].y:
                screen = 'end'
                pygame.mixer.Sound.play(snake_death)

        food1.touch([snakeHead1.x, snakeHead1.y], snakeHead1, snakeList1, )

        # Updates Score
        score_msg = font.render(str(score), True, blue)
        score_msg.set_alpha(130)
        dis.blit(score_msg, (19, 25))

        # display countdown
        if (not game_started):
            countdown_text = font.render(str(countdown), True, white)
            dis.blit(countdown_text, (dis_width / 2.02, dis_height / 2.9))

        pygame.display.update()

        # if countdown is greater than 0, wait and decrement countdown
        if (countdown > 0):
            time.sleep(1)
            countdown -= 1
        else:
            game_started = True

        clock.tick(125)

    elif screen == 'versus':
        score1 = snakeHead1.length - 1
        score2 = snakeHead2.length - 1
        score = score1 + score2
        if game_started:
            # only get input when game has started
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_over = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        snakeHead1.Set_Queued_Direction(-snake_speed, 0)
                    elif event.key == pygame.K_RIGHT:
                        snakeHead1.Set_Queued_Direction(snake_speed, 0)
                    elif event.key == pygame.K_UP:
                        snakeHead1.Set_Queued_Direction(0, -snake_speed)
                    elif event.key == pygame.K_DOWN:
                        snakeHead1.Set_Queued_Direction(0, snake_speed)

                    elif event.key == pygame.K_a:
                        snakeHead2.Set_Queued_Direction(-snake_speed, 0)
                    elif event.key == pygame.K_d:
                        snakeHead2.Set_Queued_Direction(snake_speed, 0)
                    elif event.key == pygame.K_w:
                        snakeHead2.Set_Queued_Direction(0, -snake_speed)
                    elif event.key == pygame.K_s:
                        snakeHead2.Set_Queued_Direction(0, snake_speed)

            # Makes all snake objects in snakeList follow the snake object in front of them
            if snakeHead1.isAlive:
                for i in reversed(range(snakeHead1.length)):
                    if i > 0:
                        snakeList1[i].follow(snakeList1[i - 1])

            if snakeHead2.isAlive:
                for i in reversed(range(snakeHead2.length)):
                    if i > 0:
                        snakeList2[i].follow(snakeList2[i - 1])

            # only move the snake when the game has started
            if snakeHead1.isAlive:
                for snake in snakeList1:
                    snake.move()

            if snakeHead2.isAlive:
                for snake in snakeList2:
                    snake.move()

        dis.fill(white)
        background.load_background()

        # Draw living snakes
        if snakeHead1.isAlive:
            for snake in snakeList1:
                snake.draw()

        if snakeHead2.isAlive:
            for snake in snakeList2:
                snake.draw()

        if score1 + score2 % powerup_frequency == 0 and score1 + score2 != 0:
            powerup1.initialize()

        if score % powerup_frequency == 0 and score != 0:
            powerup1.initialize()

        if powerup1.appear:
            powerup1.initialize()
            powerup1.touch([snakeHead1.x, snakeHead1.y], snakeHead1, snakeList1)
            powerup1.touch([snakeHead2.x, snakeHead2.y], snakeHead2, snakeList2)

        powerup1.check_double_food_time(snakeHead1)

        # Collision detection for snake and window edge
        if snakeHead1.x >= dis_width or snakeHead1.x < 0 or snakeHead1.y >= dis_height or snakeHead1.y < 0:
            if snakeHead1.isAlive:
                pygame.mixer.Sound.play(snake_death)
                snakeHead1.isAlive = False

        if snakeHead2.x >= dis_width or snakeHead2.x < 0 or snakeHead2.y >= dis_height or snakeHead2.y < 0:
            if snakeHead2.isAlive:
                pygame.mixer.Sound.play(snake_death)
                snakeHead2.isAlive = False

        # Stop drawing dead snake
        if not snakeHead1.isAlive:
            for snake in snakeList1:
                snake.x_change = 0
                snake.y_change = 0

        if not snakeHead2.isAlive:
            for snake in snakeList2:
                snake.x_change = 0
                snake.y_change = 0

        # Collision detection for a snake and itself
        for i in range(snakeHead1.length):
            if i != 0 and snakeList1[i].x == snakeList1[0].x and snakeList1[i].y == snakeList1[0].y:
                if snakeHead1.isAlive:
                    pygame.mixer.Sound.play(snake_death)
                    snakeHead1.isAlive = False

        for i in range(snakeHead2.length):
            if i != 0 and snakeList2[i].x == snakeList2[0].x and snakeList2[i].y == snakeList2[0].y:
                if snakeHead2.isAlive:
                    pygame.mixer.Sound.play(snake_death)
                    snakeHead2.isAlive = False

        # Collision detection for two snakes
        if snakeHead1.isAlive and snakeHead2.isAlive:
            for i in range(snakeHead2.length):
                if snakeList2[i].x == snakeList1[0].x and snakeList2[i].y == snakeList1[0].y:
                    if snakeHead1.isAlive:
                        pygame.mixer.Sound.play(snake_death)
                        snakeHead1.isAlive = False

            for i in range(snakeHead1.length):
                if snakeList1[i].x == snakeList2[0].x and snakeList1[i].y == snakeList2[0].y:
                    if snakeHead2.isAlive:
                        pygame.mixer.Sound.play(snake_death)
                        snakeHead2.isAlive = False

        if not (snakeHead1.isAlive or snakeHead2.isAlive):
            screen = 'versusEnd'

        food1.touch([snakeHead1.x, snakeHead1.y], snakeHead1, snakeList1, )

        food1.touch([snakeHead2.x, snakeHead2.y], snakeHead2, snakeList2, )

        # Updates Score
        score_msg1 = font.render(str(score2), True, blue)
        score_msg1.set_alpha(130)
        dis.blit(score_msg1, (19, 25))

        score_msg2 = font.render(str(score1), True, red)
        score_msg2.set_alpha(130)
        dis.blit(score_msg2, (dis_width - 55, 25))

        # display countdown
        if (not game_started):
            countdown_text = font.render(str(countdown), True, white)
            dis.blit(countdown_text, (dis_width / 2.02, dis_height / 2.12))

        pygame.display.update()

        # if countdown is greater than 0, wait and decrement countdown
        if (countdown > 0):
            time.sleep(1)
            countdown -= 1
        else:
            game_started = True

        clock.tick(125)

    elif screen == 'end':
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                game_over = True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    game_over = True
                    print(screen)

            if event.type == pygame.MOUSEBUTTONDOWN and dis_width / 2 - menuButtonWidth / 2 <= mouse[
                0] <= dis_width / 2 + menuButtonWidth / 2 and dis_height / 1.5 <= mouse[
                1] <= dis_height / 1.5 + menuButtonHeight:
                # Return to title functionality
                screen = 'start'
                pygame.mixer.Sound.play(click_sound)

                # Initialize snakes for next game
                gold += snakeHead1.length - 1
                snakeHead1 = Snake(equippedColor, snake1IsActive, dis)
                snakeList1 = []
                snakeList1.append(snakeHead1)
                snakeHead1.x = dis_width - 175
                snakeHead1.y = dis_height / 2

                gold += snakeHead2.length - 1
                snakeHead2 = Snake(equippedColor, snake2IsActive, dis)
                snakeList2 = []
                snakeList2.append(snakeHead2)
                snakeHead2.x = 175
                snakeHead2.y = dis_height / 2

        font = pygame.font.Font('freesansbold.ttf', 65)
        text = font.render('Game Over', True, red)
        text_block = text.get_rect()
        text_block.center = dis_width / 2, dis_height / 4

        font3 = pygame.font.Font('freesansbold.ttf', 30)
        return_text_block = text.get_rect()
        return_text_block.center = dis_width / 2 + 65, dis_height / 1.5 + 43

        mouse = pygame.mouse.get_pos()

        dis.fill(canvas_tan)

        if dis_width / 2 - menuButtonWidth / 2 <= mouse[
            0] <= dis_width / 2 + menuButtonWidth / 2 and dis_height / 1.5 <= mouse[
            1] <= dis_height / 1.5 + menuButtonHeight:
            pygame.draw.rect(dis, color_light,
                             [(dis_width / 2) - menuButtonWidth / 2, dis_height / 1.5, menuButtonWidth,
                              menuButtonHeight])
            returnText = font3.render('Return to Menu', True, (20, 15, 18))

        else:
            pygame.draw.rect(dis, color_dark, [(dis_width / 2) - menuButtonWidth / 2, dis_height / 1.5, menuButtonWidth,
                                               menuButtonHeight])
            returnText = font3.render('Return to Menu', True, (60, 50, 40))

        score_msg = font.render("Score: " + str(snakeHead1.length + snakeHead2.length - 2), True, black)
        dis.blit(score_msg, (dis_width / 2 - 125, dis_height / 2 - 45))

        dis.blit(text, text_block)
        dis.blit(returnText, return_text_block)
        pygame.display.update()


    elif screen == 'versusEnd':
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                game_over = True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    game_over = True
                    print(screen)

            if event.type == pygame.MOUSEBUTTONDOWN and dis_width / 2 - menuButtonWidth / 2 <= mouse[
                0] <= dis_width / 2 + menuButtonWidth / 2 and dis_height / 1.5 <= mouse[
                1] <= dis_height / 1.5 + menuButtonHeight:
                # Return to title functionality
                screen = 'start'
                pygame.mixer.Sound.play(click_sound)

                # Initialize snakes for next game
                gold += snakeHead1.length - 1
                snakeHead1 = Snake(equippedColor, snake1IsActive, dis)
                snakeList1 = []
                snakeList1.append(snakeHead1)
                snakeHead1.x = dis_width - 175
                snakeHead1.y = dis_height / 2

                gold += snakeHead2.length - 1
                snakeHead2 = Snake(equippedColor, snake2IsActive, dis)
                snakeList2 = []
                snakeList2.append(snakeHead2)
                snakeHead2.x = 175
                snakeHead2.y = dis_height / 2

        font = pygame.font.Font('freesansbold.ttf', 65)
        text = font.render('Game Over', True, red)
        text_block = text.get_rect()
        text_block.center = dis_width / 2, dis_height / 7

        font3 = pygame.font.Font('freesansbold.ttf', 30)
        return_text_block = text.get_rect()
        return_text_block.center = dis_width / 2 + 65, dis_height / 1.5 + 43

        mouse = pygame.mouse.get_pos()

        dis.fill(canvas_tan)

        if dis_width / 2 - menuButtonWidth / 2 <= mouse[
            0] <= dis_width / 2 + menuButtonWidth / 2 and dis_height / 1.5 <= mouse[
            1] <= dis_height / 1.5 + menuButtonHeight:
            pygame.draw.rect(dis, color_light,
                             [(dis_width / 2) - menuButtonWidth / 2, dis_height / 1.5, menuButtonWidth,
                              menuButtonHeight])
            returnText = font3.render('Return to Menu', True, (20, 15, 18))

        else:
            pygame.draw.rect(dis, color_dark, [(dis_width / 2) - menuButtonWidth / 2, dis_height / 1.5, menuButtonWidth,
                                               menuButtonHeight])
            returnText = font3.render('Return to Menu', True, (60, 50, 40))

        score_msg = font.render("Score: " + str(score2) + "-" + str(score1), True, black)
        dis.blit(score_msg, (dis_width / 2.3 - (len(str(score1)) + len(str(score2))) * 43, dis_height / 2 - 150))

        if (score2 > score1):
            winner_msg = font.render("Left Player Wins!", True, blue)
            dis.blit(winner_msg, (dis_width / 4.5, dis_height / 2 - 35))

        elif (score1 > score2):
            winner_msg = font.render("Right Player Wins!", True, red)
            dis.blit(winner_msg, (dis_width / 4.78, dis_height / 2 - 35))

        elif (score2 == score1):
            winner_msg = font.render("Draw!", True, black)
            dis.blit(winner_msg, (dis_width / 2.4, dis_height / 2 - 35))

        dis.blit(text, text_block)
        dis.blit(returnText, return_text_block)
        pygame.display.update()

    elif screen == 'shop':

        dis.fill(canvas_tan)

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                game_over = True

            if event.type == pygame.MOUSEBUTTONDOWN and dis_width / 2 - menuButtonWidth / 2 <= mouse[
                0] <= dis_width / 2 + menuButtonWidth / 2 and dis_height / 3 <= mouse[
                1] <= dis_height / 3 + menuButtonHeight:
                screen = 'start'
                pygame.mixer.Sound.play(click_sound)

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    screen = 'start'

            if event.type == pygame.MOUSEBUTTONDOWN and (
                    gold >= 10 or boughtBlue) and dis_width / 2 - buyButtonWidth / 2 <= mouse[
                0] <= dis_width / 2 + buyButtonWidth / 2 and dis_height / 1.4 <= mouse[
                1] <= dis_height / 1.4 + playButtonHeight:
                if equippedColor != blue:
                    snakeHead1.base_color = blue
                    snakeHead2.base_color = blue
                    equippedColor = blue
                else:
                    snakeHead1.base_color = defaultColor
                    snakeHead2.base_color = defaultColor
                    equippedColor = defaultColor
                if not boughtBlue:
                    gold -= 10
                    boughtBlue = True

            if event.type == pygame.MOUSEBUTTONDOWN and (
                    gold >= 10 or boughtRed) and dis_width / 2 - buyButtonWidth / 2 <= mouse[
                0] <= dis_width / 2 + buyButtonWidth / 2 and dis_height / 1.6 <= mouse[
                1] <= dis_height / 1.6 + buyButtonHeight:
                if equippedColor != red:
                    snakeHead1.base_color = red
                    snakeHead2.base_color = red
                    equippedColor = red
                else:
                    snakeHead1.base_color = defaultColor
                    snakeHead2.base_color = defaultColor
                    equippedColor = defaultColor
                if not boughtRed:
                    gold -= 10
                    boughtRed = True

        font4 = pygame.font.Font('freesansbold.ttf', 20)
        buyBlueText = font4.render('Blue Snake', True, blue)
        font2 = pygame.font.Font('freesansbold.ttf', 35)
        shopText = font2.render('Shop', True, blue)
        buy_blue_text_block = shopText.get_rect()
        buy_blue_text_block.center = dis_width / 2 - 15, dis_height / 1.29 - 5

        blueCostText = goldFont.render('10', True, black)
        blueCost_text_block = blueCostText.get_rect()
        blueCost_text_block.center = dis_width / 2 - 130, dis_height / 1.29 - 12
        if equippedColor == blue:
            pygame.draw.circle(dis, light_yellow, radius=14,
                               center=[(dis_width / 2) - buyButtonWidth + 91, dis_height / 1.4 + 21])
        else:
            pygame.draw.circle(dis, yellow, radius=14,
                               center=[(dis_width / 2) - buyButtonWidth + 71, dis_height / 1.4 + 21])

        font4 = pygame.font.Font('freesansbold.ttf', 20)
        buyRedText = font4.render('Blue Snake', True, blue)
        buy_red_text_block = shopText.get_rect()
        buy_red_text_block.center = dis_width / 2 - 15, dis_height / 1.29 - 54

        redCostText = goldFont.render('10', True, black)
        redCost_text_block = blueCostText.get_rect()
        redCost_text_block.center = dis_width / 2 - 130, dis_height / 1.29 - 61
        if equippedColor == red:
            pygame.draw.circle(dis, light_yellow, radius=14,
                               center=[(dis_width / 2) - buyButtonWidth + 91, dis_height / 1.4 - 28])
        else:
            pygame.draw.circle(dis, yellow, radius=14,
                               center=[(dis_width / 2) - buyButtonWidth + 71, dis_height / 1.4 - 28])

        mouse = pygame.mouse.get_pos()

        if dis_width / 2 - buyButtonWidth / 2 <= mouse[0] <= dis_width / 2 + buyButtonWidth / 2 and dis_height / 1.4 <= \
                mouse[1] <= dis_height / 1.4 + buyButtonHeight:
            pygame.draw.rect(dis, color_light,
                             [(dis_width / 2) - buyButtonWidth / 2, dis_height / 1.4, buyButtonWidth, buyButtonHeight])
            buyBlueText = font4.render('Blue Snake', True, light_blue)

        else:
            pygame.draw.rect(dis, color_dark,
                             [(dis_width / 2) - buyButtonWidth / 2, dis_height / 1.4, buyButtonWidth, buyButtonHeight])
            buyBlueText = font4.render('Blue Snake', True, blue)

        if dis_width / 2 - buyButtonWidth / 2 <= mouse[0] <= dis_width / 2 + buyButtonWidth / 2 and dis_height / 1.6 <= \
                mouse[1] <= dis_height / 1.6 + buyButtonHeight:
            pygame.draw.rect(dis, color_light,
                             [(dis_width / 2) - buyButtonWidth / 2, dis_height / 1.6, buyButtonWidth, buyButtonHeight])
            buyRedText = font4.render('Red Snake', True, light_red)

        else:
            pygame.draw.rect(dis, color_dark,
                             [(dis_width / 2) - buyButtonWidth / 2, dis_height / 1.6, buyButtonWidth, buyButtonHeight])
            buyRedText = font4.render('Red Snake', True, red)

        font = pygame.font.Font('freesansbold.ttf', 65)
        shopTitle = font.render('Shop', True, gold)
        shop_title_block = shopTitle.get_rect()
        shop_title_block.center = dis_width / 2, dis_height / 8

        returnTextShop = font.render("Return to Menu", True, (60, 50, 40))
        font3 = pygame.font.Font('freesansbold.ttf', 30)

        if dis_width / 2 - menuButtonWidth / 2 <= mouse[0] <= dis_width / 2 + menuButtonWidth / 2 and dis_height / 3 <= \
                mouse[1] <= dis_height / 3 + menuButtonHeight:
            pygame.draw.rect(dis, color_light,
                             [(dis_width / 2) - menuButtonWidth / 2, dis_height / 3, menuButtonWidth, menuButtonHeight])
            returnTextShop = font3.render('Return to Menu', True, (20, 15, 18))

        else:
            pygame.draw.rect(dis, color_dark,
                             [(dis_width / 2) - menuButtonWidth / 2, dis_height / 3, menuButtonWidth, menuButtonHeight])
            returnTextShop = font3.render('Return to Menu', True, (60, 50, 40))

        gold_msg = goldFont.render("Gold: " + str(gold), True, gold_color)

        dis.blit(buyBlueText, buy_blue_text_block)
        dis.blit(returnTextShop, (dis_width / 2 - 115, dis_height / 2.84))
        dis.blit(gold_msg, (25, 25))
        dis.blit(shopTitle, shop_title_block)
        dis.blit(buyRedText, buy_red_text_block)
        if not boughtBlue:
            dis.blit(blueCostText, blueCost_text_block)
        if not boughtRed:
            dis.blit(redCostText, redCost_text_block)
        pygame.display.update()

pygame.quit()
quit()
