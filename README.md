# CS205 Final Project

This project ic created by Logan Scholfield, Tucker Paron, Sam Gusick and Doug Cilli

This project is a re-make of the classic snake game where a snake player controlled snake
moves around a 2D space and collects food. As food is collected the snake increases in length.
The objective of the game is to get the longest snake.

To run the game pygames and numpy need to be installed
use the following commands to install these.

    pip install pygames
    pip install numpy


To run the program run main.py. Once the game is running, press the space bar to start the game. Use the arrow keys to change
the direction the snake is moving. Avoid hitting the walls or yourself and see how long
you can get. Once the game ends, press space again to exit.


