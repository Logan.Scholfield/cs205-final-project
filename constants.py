
'''
--------------------------
----------COLORS----------
--------------------------
'''
#general colors
white = (255, 255, 255)
yellow = (255, 255, 102)
black = (0, 0, 0)
red = (213, 50, 80)
green = (0, 255, 0)
blue = (50, 153, 213)
canvas_tan = (220,209,191)
green_grass_1 = (33.73,49.02,27.45)
green_grass_2 = (42,61.18,34.51)
green_grass_3 = (32.55,51.76,24.71)
gold_color = (191, 179, 40)

# light shade of the button
color_light = (130, 170, 170)

# dark shade of the button
color_dark = (60, 100, 100)

# nice light shades
light_blue = (173,216,255)
light_red = (255,204,203)
light_yellow = (255,255,224)

#powerup colors
bomb_yellow = (255, 175, 0)

# button colors
button_color = (121, 202, 224)
highlighted_button_color = (173, 238, 255)
button_text_color = (60, 128, 181)



'''
--------------------------
---------POWERUPS---------
--------------------------
'''
number_powerups = 2


'''
--------------------------
-------GENERAL GAME-------
--------------------------
'''
game_over = False
countdown_duration = 3
# Changing this will break the game
snake_speed = 1


'''
--------------------------
-----------SHOP-----------
--------------------------
'''
boughtBlue = False
boughtRed = False
gold = 0
equippedColor = white
defaultColor = white


'''
--------------------------
----------DISPLAY---------
--------------------------
'''


#display withds for main screen size
dis_width = 1000
dis_height = 550
size = [dis_width, dis_height]

#blocks size used throught game
block_size = 25

#screen type origional set
screen = 'start'


'''
--------------------------
---------BUTTONS----------
--------------------------
'''
menuButtonWidth = 275
menuButtonHeight = 50

playButtonWidth = 200
playButtonHeight = 65

buyButtonWidth = 200
buyButtonHeight = 40

shopButtonWidth = 350
shopButtonHeight = 120
shopButtonXlocation = dis_width/1.5
shopButtonYlocation = dis_height/1.4

coopButtonWidth = 350
coopButtonHeight = 120
coopButtonXlocation = dis_width/1.5
coopButtonYlocation = dis_height/2.3

soloButtonWidth = 350
soloButtonHeight = 120
soloButtonXlocation = dis_width/3.5
soloButtonYlocation = dis_height/2.3

versusButtonWidth = 350
versusButtonHeight = 120
versusButtonXlocation = dis_width/3.5
versusButtonYlocation = dis_height/1.4

helpButtonWidth = 130
helpButtonHeight = 45
helpButtonXlocation = dis_width/2.05
helpButtonYlocation = dis_height/1.08

'''
--------------------------
---------POWERUPS----------
--------------------------
'''
powerup_frequency = 5


'''
--------------------------
---------POWERUPS----------
--------------------------
'''
title_indent = 30
sentence_indent = 18
mini_block = block_size/4

'''
--------------------------
---------MUSIC----------
--------------------------
'''
play_music = True