'''
Food class for randomly generated food objects
'''
from constants import *
import constants as c
from snake import *
import random
import pygame

pygame.init()
dis = pygame.display.set_mode((c.dis_width, c.dis_height))

eat_sound = pygame.mixer.Sound('Eat Sound 2.wav')

class Food:
    #locations array for later use
    locations = []


    '''
    Food objects have:
        An array for x and y cords
        Color for foods color
    '''
    def __init__(self, window):
        self.loc = self.get_loc()
        self.food_color = self.get_random_color()
        self.food_type = self.get_new_type()
        self.window = window

    '''
    Gets a random location for the snake when called
    Rounds to nearest block_size from constants file
    Returns array for both x and y variables
    Takes in nothing
    Returns loc array
    '''
    def get_loc(self):
        loc = [round(random.randrange(0, (size[0]-block_size))), round(random.randrange(0, (size[1]-block_size)))]
        loc[0] = round(loc[0]/block_size)*block_size
        loc[1] = round(loc[1]/block_size)*block_size
        return loc


    '''
    Resets, or does not reset foods values depending on reset flag
    Draws the food object
    Takes in reset flag
    returns nothing
    '''
    def initialize(self, reset):
        if reset:
            self.loc = self.get_loc()
            self.food_color = self.get_random_color()
        pygame.draw.rect(self.window, self.food_color, pygame.Rect(self.loc[0],self.loc[1],block_size,block_size))


    '''
    Checks if a food object is touching the snake head and resets food if it is
    Takes in the snake_loc for the snake heads location
    Returns nothing
    '''
    def touch(self, snake_loc, snake, snakeList):
        if snake_loc == self.loc:
            newSnake = Snake(self.food_color, True, dis)
            lastSnake = snakeList[snake.length-1]
            newSnake.x = lastSnake.x - 25 * lastSnake.x_change
            newSnake.y = lastSnake.y - 25 * lastSnake.y_change
            newSnake.x_change = lastSnake.x_change
            newSnake.y_change = lastSnake.y_change
            snakeList.append(newSnake)
            pygame.mixer.Sound.play(eat_sound)
            if snake.double_food:
                newSnake = Snake(self.food_color, True, dis)
                newSnake.x = lastSnake.x - 50 * lastSnake.x_change
                newSnake.y = lastSnake.y - 50 * lastSnake.y_change
                newSnake.x_change = lastSnake.x_change
                newSnake.y_change = lastSnake.y_change
                snakeList.append(newSnake)
            self.initialize(True)
            snake.length += 1
            if snake.double_food:
                snake.length += 1
        else:
            self.initialize(False)


    '''
    gets a random color from 5 pre created  color options
    Takes in nothing
    returns a color
    '''
    def get_random_color(self):
        int = round(random.randrange(1, 6))
        if int == 1:
            color = (255,0,0)
        elif int == 2:
            color = (255,155,0)
        elif int == 3:
            color = (255,255,0)
        elif int == 4:
            color = (34,139,34)
        elif int == 5:
            color = (173,216,230)
        return color


    ''' 
    Gets a new fruit image for food
    Currently not fully developed or in use
    For later use
    Takes in nothing
    Returns string for which food image will be used
    '''
    def get_new_type(self):
        int = round(random.randrange(1, 6))
        if int == 1:
            type = 'images/fruits/apple.png'
        elif int == 2:
            type = 'images/fruits/banana.png'
        elif int == 3:
            type = 'images/fruits/kiwi.png'
        elif int == 4:
            type = 'images/fruits/pear.png'
        elif int == 5:
            type = 'images/fruits/plum.png'
        return type
