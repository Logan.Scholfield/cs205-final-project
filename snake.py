import pygame
from constants import *

class Snake:
    
    def __init__(self, color, active, display) -> None:
        self.display = display
        self.base_color = color
        self.x = dis_width/2
        self.y = dis_height/2
        self.queued_x_change = 0
        self.queued_y_change = 0
        self.x_change = 0       
        self.y_change = 0
        self.length = 1
        self.powerup = None
        self.double_food = False
        self.isActive = active
        self.isAlive = True
        self.last_x_change = 0
        self.last_y_change = 0

    def Set_Direction(self,directionX, directionY):
        if (not self.isActive):
            return
        self.x_change = directionX
        self.y_change = directionY

    def Set_Queued_Direction(self,directionX, directionY):
        if (not self.isActive):
            return
        self.queued_x_change = directionX
        self.queued_y_change = directionY
    
    def move(self):
        if (not self.isActive):
            return
        if self.x % 25 == 0 and self.y % 25 == 0:
            self.last_x_change = self.x_change
            self.last_y_change = self.y_change
            self.Set_Direction(self.queued_x_change,self.queued_y_change)
        self.x += self.x_change
        self.y += self.y_change

    def follow(self,snake):
        if (not self.isActive):
            return

        self.queued_x_change = snake.x_change
        self.queued_y_change = snake.y_change
        # self.x_change = snake.last_x_change
        # self.y_change = snake.last_y_change


    def draw(self):
        pygame.draw.rect(self.display, self.base_color, [self.x, self.y, block_size, block_size])
 

    def set_double_food(self, change_bool):
            self.double_food = change_bool


    def get_loc(self):
        return [self.x, self.y]
