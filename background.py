from constants import *
import pygame


class Background:
    def __init__(self, display):
        self.display = display
        self.x = 0
        self.y = 0
        self.color = green_grass_2

    def switch_color(self):
        if self.color == green_grass_2:
            self.color = green_grass_3
        else:
            self.color = green_grass_2

    def load_background(self):
        for temp_x in range(0,dis_width, 25):
            self.switch_color()
            for temp_y in range(0,dis_height,25):
                self.switch_color()
                self.x = temp_x
                self.y = temp_y
                pygame.draw.rect(self.display, self.color, [self.x, self.y, block_size, block_size])



